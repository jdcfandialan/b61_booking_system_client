import {Button} from 'react-bootstrap';
import Router from 'next/router'

export default function DeleteButton({courseId}){
	
	const deleteCourse = (courseId) => {

		fetch(`http://localhost:4000/api/courses/${courseId}`, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Router.reload();
			}

			else{
				Router.push('/error');
			}
		})
	}

	return(
		<Button variant="danger" onClick={() => deleteCourse(courseId)}>Delete</Button>
	)
}