import {useContext} from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function EnrollButton({courseId}){

	const {user} = useContext(UserContext);
	
	const enrollUser = (courseId) => {
		fetch(`http://localhost:4000/api/courses/${courseId}/enrollments`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire('Enrollment successful!');
			}

			else{
				Swal.fire('Oops...', 'Something went wrong!', 'error');
			}
		})
	}

	return (
		(user.id !== null && user.isAdmin === false) ? <Button variant="success" onClick={() => enrollUser(courseId)}>Enroll</Button> : null
	)
}