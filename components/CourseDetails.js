import {Jumbotron, Table} from 'react-bootstrap';
import Link from 'next/link'

export default function CourseDetails({courseData}){
	
	return (
		<React.Fragment>
			<Jumbotron>
				<h1>{courseData.name}</h1>
				<h2>{courseData.description}</h2>
				<p>PHP {courseData.price}</p>
			</Jumbotron>

			<h3>List of Enrollees</h3>
			<Table striped hover bordered>
				<thead>
					<tr>
						<th>Student ID</th>
					</tr>
				</thead>

				<tbody>
					{
						courseData.enrollees.map(enrollee => {
							return (
								<tr key={enrollee.userId}>
									<td>
										<Link href="/users/[id]" as={`/users/${enrollee.userId}`}>
											<a>{enrollee.userId}</a>
										</Link>
									</td>
								</tr>
							)
						})
					}
				</tbody>
			</Table>
		</React.Fragment>
	)
}