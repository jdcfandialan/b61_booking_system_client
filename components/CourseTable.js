import Link from 'next/link';
import {Table} from 'react-bootstrap';
import UpdateButton from './UpdateButton';
import DeleteButton from './DeleteButton';

export default function CourseTable({course}){
	return(
		<tr>
			<td>
				<Link href="/courses/[id]" as={`/courses/${course._id}`}>
					<a>{course.name}</a>
				</Link>
			</td>
			<td>{course.description}</td>
			<td>PHP {course.price}</td>
			<td><UpdateButton courseId={course._id} /><DeleteButton courseId={course._id} /></td>
		</tr>
	)
}