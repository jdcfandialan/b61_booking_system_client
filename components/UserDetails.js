import {Jumbotron, Table} from 'react-bootstrap';
import Link from 'next/link';

export default function UserDetails({userData}){
	return (
		<React.Fragment>
			<Jumbotron>
				<h1>{userData.firstName} {userData.lastName}</h1>
				<h2>{userData.email}</h2>
			</Jumbotron>

			<h3>Courses enrolled</h3>
			<Table>
				<thead>
					<tr>
						<th>Course ID</th>
					</tr>
				</thead>

				<tbody>
					{
						userData.enrollments.map(enrollment => {
							return (
								<tr key={enrollment.courseId}>
									<td>
										<Link href="/courses/[id]" as={`/courses/${enrollment.courseId}`}>
											<a>{enrollment.courseId}</a>
										</Link>
									</td>
								</tr>
							)
						})
					}
				</tbody>
			</Table>
		</React.Fragment>
	)
}