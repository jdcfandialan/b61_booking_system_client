import {Card} from 'react-bootstrap';
import EnrollButton from './EnrollButton';
import Link from 'next/link'

export default function CourseCard({course}){

	return (			
		<Card>
			<Card.Body>
				<Card.Title>
					<Link href={'/courses/[id]'} as={`/courses/${course._id}`}>
						<a>{course.name}</a>
					</Link>
				</Card.Title>
				<Card.Subtitle>{course.description}</Card.Subtitle>
				<Card.Text>Price: PHP {course.price}</Card.Text>
				<EnrollButton courseId = {course._id} />
			</Card.Body>
		</Card>
	)
}