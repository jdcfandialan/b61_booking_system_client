import {useRouter} from 'next/router';
import {Spinner} from 'react-bootstrap';
import UserDetails from '../../components/UserDetails';

export default function user({userData}){

	const router = useRouter;

	if(router.isFallback){
		return (
			<React.Fragment>
				<span>Fetching course details from API...</span>
				<Spinner animation="border" variant="primary" />
			</React.Fragment>
		)
	}

	else return <UserDetails userData={userData} />
}

export async function getStaticPaths(){

	const res = await fetch('http://localhost:4000/api/users');
	const data = await res.json();
	const paths = data.map(datum => {
		return {
			params: {
				id: datum._id
			}
		}
	})

	return {
		paths,
		fallback: true
	}
}

export async function getStaticProps({params}){
	
	const res = await fetch(`http://localhost:4000/api/users/${params.id}`);
	const userData = await res.json();

	return {
		props: {
			userData
		}
	}
}