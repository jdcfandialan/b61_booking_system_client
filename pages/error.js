import {Jumbotron} from 'react-bootstrap'

export default function Error(){
	return (
		<Jumbotron>
			<h1>Oops!</h1>
			<p>something went wrong!</p>
		</Jumbotron>
	)
}