import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Router from 'next/router';
import {GoogleLogin} from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){
	const [email, setEmail] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [isActive, setIsActive] = useState(false);

    const {setUser} = useContext(UserContext);

	useEffect(() => {
		if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNumber.length === 10)){
			setIsActive(true);
		}

		else setIsActive(false);

	}, [password1, password2, mobileNumber]);

	function registerUser(e){
		e.preventDefault();
		
		fetch('http://localhost:4000/api/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1,
                mobileNumber: mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
            //if registration is successful, redirect to login
            if(data === true){
				Router.push('/login')
            }
            else{//redirect to an error page otherwise
				Router.push('/error')
            }
        })

	}

    const captureLoginResponse = response => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }

        fetch('http://localhost:4000/api/users/verifyGoogleIdToken', payload)
        .then(res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })

                Router.push('/courses')
            }

            else{
                if(data.error === 'google-auth-login'){
                    Swal.fire('Google Auth Error', 'Google authentication failed', 'error');
                }

                else if(data.error === 'login-type-error'){
                    Swal.fire('Login Type Error', 'Invalid login procedure!', 'error');
                }
            }
        })
    }

	return (
		<React.Fragment>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type='text' value={firstName} onChange={e => setFirstName(e.target.value)} required></Form.Control>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type='text' value={lastName} onChange={e => setLastName(e.target.value)} required></Form.Control>
				</Form.Group>

				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control type='email' value={email} onChange={e => setEmail(e.target.value)} required></Form.Control>
				</Form.Group>

                <Form.Group>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control type='text' value={mobileNumber} placeholder="9171234567" onChange={e => setMobileNumber(e.target.value)} required></Form.Control>
                </Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type='password' value={password1} onChange={e => setPassword1(e.target.value)} required></Form.Control>
				</Form.Group>

				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control type='password' value={password2} onChange={e => setPassword2(e.target.value)} required></Form.Control>
				</Form.Group>

				{
					(isActive === true) ? <Button type="submit" variant="success">Submit</Button> : <Button type="submit" variant="success" disabled>Submit</Button>
				}
			</Form>

			<p className="mt-2">or</p>

			 <GoogleLogin clientId='68746645342-at8lh921jbdjt5jqcbco58p719efkr4j.apps.googleusercontent.com' onSuccess={captureLoginResponse} onFailure={captureLoginResponse} cookiePolicy={'single_host_origin'} render ={renderProps => (
                    <Button className="mb-3" variant="outline-primary" onClick={renderProps.onClick} disabled={renderProps.disabled} >Login using Google Account</Button>
                )} />
		</React.Fragment>
	)
}