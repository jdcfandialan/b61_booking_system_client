import {useState, useEffect} from 'react';
import Head from 'next/head'
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {UserProvider} from '../UserContext';
import {Container} from 'react-bootstrap'
import NavBar from '../components/NavBar';

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
  		id: null,
  		isAdmin: null
  	});

  	const unsetUser = () => {
  		localStorage.clear();
  		
  		setUser({
  			id: null,
  			isAdmin: null
  		});
  	}

	useEffect(() => {
		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data._id){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			}

			else{
				id: null
				isAdmin: null
			}
		});
	}, [user.id]);

	return (
		<React.Fragment>
			<Head>
				<title>B61 Booking</title>	
			</Head>

			<UserProvider value = {{user, setUser, unsetUser}} >
				<NavBar />
				<Container>
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
	)
}

export default MyApp;
