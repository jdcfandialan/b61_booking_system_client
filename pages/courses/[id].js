import {useRouter} from 'next/router';
import {Spinner} from 'react-bootstrap';
import CourseDetails from '../../components/CourseDetails';

export default function course({courseData}){
	
	const router = useRouter;

	if(router.isFallback){
		return (
			<React.Fragment>
				<span>Fetching course details from API...</span>
				<Spinner animation="border" variant="primary" />
			</React.Fragment>
		)
	}

	else return <CourseDetails courseData={courseData} />
}

export async function getStaticPaths(){
	
	const res = await fetch('http://localhost:4000/api/courses');
	const data = await res.json();
	const paths = data.map(datum => {
		
		return {
			params: {
				id: datum._id,
			}
		}
	})

	return {
		paths,
		fallback: true
	}
}

export async function getStaticProps({params}){
	
	const res = await fetch(`http://localhost:4000/api/courses/${params.id}`);
	const courseData = await res.json();

	return {
		props: {
			courseData
		}
	}
}