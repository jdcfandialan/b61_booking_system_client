import {useState, useEffect, useContext} from 'react';
import {Form, Button, Alert, Jumbotron} from 'react-bootstrap';
import Router from 'next/router';
import UserContext from '../../UserContext';

export default function Create(){
	const {user} = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(100);
	const [notify, setNotify] = useState(false);
	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(user.isAdmin !== true){
			Router.push('/courses')
		}
	}, []);

	useEffect(()=>{
		if(name.length < 20 && description.length < 200){
			setIsActive(true);
		}

		else{
			setIsActive(false);
		}
	}, [name, description]);

	function createCourse(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name:name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Router.push('/courses');
			}

			else{
				setNotify(true);
			}
		})
	}
	if(user.isAdmin){
		return (
			<React.Fragment>
				<Form onSubmit={(e)=>createCourse(e)}>
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" name={name} onChange={(e) => setName(e.target.value)} />
						
						{
							(name.length < 20) ? null : <Alert variant="danger">Course name exceeded maximum characters allowed!</Alert>
						}

					</Form.Group>

					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control as="textarea" row="3" name={description} onChange={(e) => setDescription(e.target.value)} />

						{
							(description.length < 200) ? null : <Alert variant="danger">Course description exceeded maximum characters allowed!</Alert>
						}
					</Form.Group>

					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" min={100} name={price} onChange={e => setPrice(e.target.value)} />
					</Form.Group>
					
					{
						(isActive) ? <Button variant="success" type="submit">Submit</Button> : <Button variant="success" type="submit" disabled>Submit</Button>
					}
				</Form>
				{
					(notify) ? <Alert variant="danger">Error in new course creation!</Alert> : null
				}
			</React.Fragment>
		)
	}

	else{
		return(
			<Jumbotron>UNAUTHORIZED ACCESS! Redirecting now.</Jumbotron>
		)
	}
}