import {useState, useEffect, useContext} from 'react';
import CourseCard from '../../components/CourseCard';
import {Row, Col, Table, Jumbotron} from 'react-bootstrap';
import UserContext from '../../UserContext';
import CourseTable from '../../components/CourseTable';

export default function index({data}){

	const {user} = useContext(UserContext);

	return(
		<Row className="d-flex align-items-center justify-content-center">
			{

				(data.length === 0) ? 
				
				<Jumbotron>
					<h1>No courses available!</h1>
				</Jumbotron>
				
				:
				
				(user.isAdmin) ?
				
				<Table bordered striped hover variant="dark" className="mt-3">
					<thead>
						<tr>
							<th>Course Name</th>
							<th>Description</th>
							<th>Price</th>
						</tr>
					</thead>

					<tbody>
						{
							data.map((course)=>{
								return(
									<CourseTable key={course._id} course={course} />
								)
							})	
						}
					</tbody>
				</Table>

				:

				data.map((course) => {
					return(

						<Col lg={4} className="pt-3" key={course._id}>
							<CourseCard course={course} />
						</Col>
					)
				})
			}
		</Row>
	)
}

export async function getServerSideProps(){
	
	const res = await fetch('http://localhost:4000/api/courses');

	const data = await res.json();

	return {
		props :{
			data
		}
	}
}