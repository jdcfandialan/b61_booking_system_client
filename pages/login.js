import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {GoogleLogin} from 'react-google-login';
import Router from 'next/router';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){
	const {setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	function authenticate(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
		.then(data => {
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken);
				fetch('http://localhost:4000/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})
					Router.push('/courses');
				})
			}

			else Router.push('/error');
		})
	}
	
	const captureLoginResponse = (response) => {
		//console.log(response);

		const payload = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		}

		fetch('http://localhost:4000/api/users/verifyGoogleIdToken', payload)
		.then(res => res.json())
		.then(data => {
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken);
				
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
				
				Router.push('/courses');
			}

			else{
				if(data.error === 'google-auth-login'){
					Swal.fire('Google Auth Error', 'Google authentication failed', 'error');
				}

				else if(data.error === 'login-type-error'){
					Swal.fire('Login Type Error', 'Invalid login procedure!', 'error');
				}
			}
		})
	}

	return (
		<React.Fragment>
			<Form onSubmit={(e) => authenticate(e)} className="mb-2">
	            <Form.Group>
	                <Form.Label>Email</Form.Label>
	                <Form.Control type="email" value={email} onChange={(e)=>setEmail(e.target.value)} />
	            </Form.Group>

	            <Form.Group>
	                <Form.Label>Password</Form.Label>
	                <Form.Control type="password" value={password} onChange={(e)=>setPassword(e.target.value)} />
	            </Form.Group>
	            
	            <Button variant="success" type="submit">Submit</Button>
	        </Form>

	        <GoogleLogin clientId='68746645342-at8lh921jbdjt5jqcbco58p719efkr4j.apps.googleusercontent.com' onSuccess={captureLoginResponse} onFailure={captureLoginResponse} cookiePolicy={'single_host_origin'} render ={renderProps => (
					<Button variant="outline-primary" onClick={renderProps.onClick} disabled={renderProps.disabled} >Login using Google Account</Button>
	        	)} />
        </React.Fragment>
	)
}